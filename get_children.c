#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
    // //make sure 2 arguments
    if(argc != 1){
        printf(1, "The export command has 1 arguments. %d given. \n", argc-1);
        exit();
    }
    if(fork()==0){
        if(fork()==0){
            if(fork()==0){
                sleep(100);
            }
            else{
                wait();
            }
        }
        else{
            wait();
        }
    }
    else{
        sleep(10);
        int a = get_children(getpid());
        wait();
    }
    exit();
}
