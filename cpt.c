#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"

void copy_files(const char* source_path, char* destination_path)
{
    int source_fd;
    int destination_fd;
    struct stat source_status;

    if ((source_fd = open(source_path, O_RDONLY)) < 0)
    {
        printf(2, "cpt: cannot open %s\n", source_path);
        return;
    }
    
    if ((destination_fd = open(destination_path, O_WRONLY)) < 0)
    {
        printf(2, "cpt: %s not found. Creating one...\n", destination_path);
        open(destination_path, O_CREATE);
        destination_fd = open(destination_path, O_WRONLY);
    }

    fstat(source_fd, &source_status);

    char* buffer = (char*)malloc(source_status.size * sizeof(char));
    
    uint read_size = read(source_fd, buffer, source_status.size);
    write(destination_fd, buffer, read_size);

    free(buffer);
    close(source_fd);
    close(destination_fd);
}

void fill_file_with_input(char* destination_path)
{
    const uint buffer_size = 100;
    int destination_fd;

    if ((destination_fd = open(destination_path, O_WRONLY)) < 0)
    {
        printf(2, "cpt: cannot open %s\n", destination_path);
        return;
    }

    char buffer[buffer_size];
    memset(buffer, 0, buffer_size);
    gets(buffer, buffer_size);
    write(destination_fd, buffer, buffer_size);
    close(destination_fd);
}

int main(int argc, char *argv[])
{
    if (argc == 1)
        printf(2, "cpt: at least one argument\n");
        
    else if (argc == 2)
        fill_file_with_input(argv[1]);

    else if (argc == 3)
        copy_files(argv[1], argv[2]);

    exit();
}
