#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

int
sys_sleep_proc(void)
{
  // struct rtcdate *start_time;
  // cmostime(start_time);

  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    // sleep(&ticks, &tickslock);
    void *chan = &ticks;
    struct spinlock *lk = &tickslock;
    struct proc *p = myproc();
    
    if(p == 0)
      panic("sleep");

    if(lk == 0)
      panic("sleep without lk");
    if(lk != &ptable.lock){  
      acquire(&ptable.lock); 
      release(lk);
    }
    p->chan = chan;
    p->state = SLEEPING;

    sched();

    p->chan = 0;

    if(lk != &ptable.lock){ 
      release(&ptable.lock);
      acquire(lk);
    }
  }
  release(&tickslock);

  // struct rtcdate *end_time;
  // cmostime(end_time);
  // cprintf("%d %d\n", end_time->second , start_time->second);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_get_children(void)
{
  // panic("fuck");
  int pid;
  struct proc *p;
  if(argint(0, &pid) < 0)
    return -1;

  acquire(&ptable.lock);

  int processes[1024];
  processes[0] = pid;
  int size = 1;
  int pointer = 0;
  
  for(;pointer!=size;pointer+=1){
    int parent_id = processes[pointer];
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
        if(p->parent->pid == parent_id)
        {
          processes[size] = p->pid;
          cprintf("%d\n",p->pid);
          size +=1;
        }
    }
  }

  release(&ptable.lock);
  return pid;
}