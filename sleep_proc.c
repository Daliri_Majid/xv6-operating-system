#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
    //make sure 2 arguments
    if(argc != 2){
        printf(1, "The export command has 1 arguments. %d given. \n", argc-1);
        exit();
    }
    int sleep_time =100 * atoi(argv[1]);
    sleep_proc(sleep_time);
    exit();
}
